﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public MoveController moveDavid;
    public MoveController moveCata;
    public Checker chekerDavid;
    public Checker chekerCata;


    private void CheckingMode()
    {
        if (chekerDavid.CubeCheck() != Cube_Action.No_Action)
        {
            MoveTrigger(moveDavid,chekerDavid.CubeCheck(),chekerDavid.LoveCheck());
        }
    }

    private void MoveTrigger(MoveController TargetMove,Cube_Action action,bool lookMyLove)
    {
        switch (action)
        {
            case Cube_Action.Move_Forward:
                if (lookMyLove)
                {
                    
                }
                else
                {
                    TargetMove.MoveNormal();
                }

                break;
            case Cube_Action.Turn_Left:
                break;
            case Cube_Action.Turn_Right:
                break;
            case Cube_Action.Turn_Down:
                break;
            default:
                break;
        }
    }


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            StartGameplay();
        }
    }
    void Start()
    {

    }

    private void StartGameplay()
    {
        CheckingMode();
    }

}
