﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveController : MonoBehaviour
{
    private bool canMove = true;
    private Checker checker;
    private float winMove = 0.75f;
    private float delayMove = 0.01f;
    private float nomalMove = 1f;


    public void MoveNormal()
    {
        StartCoroutine(MoveForward());
    }
    IEnumerator MoveForward()
    {
        float distanceMove = 0;
        while (distanceMove < nomalMove)
        {
            transform.Translate(transform.forward * 0.1f, Space.World);
            distanceMove += 0.1f;
            yield return new WaitForSeconds(delayMove);
        }
        yield return new WaitForSeconds(1f);
    }


}
