﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeCharacter : MonoBehaviour
{   
    [SerializeField]
    private Cube_Action action;

    public Cube_Action getAction()
    {
        return action;
    } 

}
