﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checker : MonoBehaviour
{
    MoveController moveController;

    private void Awake()
    {
        moveController = transform.GetComponent<MoveController>();
    }


    public bool LoveCheck()
    {
        RaycastHit hitFind;

        if (Physics.Raycast(transform.position, transform.forward, out hitFind, 2))
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    public Cube_Action CubeCheck()
    {
        RaycastHit moveRay;

        if (Physics.Raycast(transform.position + transform.forward, -transform.up, out moveRay))
        {
            return moveRay.transform.GetComponent<CubeCharacter>().getAction();
        }
        else
        {
            return Cube_Action.No_Action;
        }

    }
}
